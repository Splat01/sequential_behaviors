#### Predator/prey sequential behavioral response model description
#### Version 3

This is a model that looks at prey and predator fitness in a system with multiple patches and multiple prey per patch (showing schooling behavior). Each patch has an open area for foraging and a refuge for the prey to travel between. The predator is able to move among patches, as well as leave the patches to some general "environment".  

The model will determine fitness based on movement rates (prey: refuge <-> open; pred: patches <-> some other environment). We will use an adaptive dynamic approach to identify the impact each player has on the decisions (spatial movements) of the other.  

**Next steps**  

Convert pickle to shelving

DONE: Add rate graphs for two new states (a_out_attack, b_out_attack)
DONE Add code that saves plots as .png files to a unique directory  
DONE Graph sum of a\_out + b\_in for proportion of time spent out/in  
Done:Remove global variables  
DONE: modify mu() (now is variable called pred\_kill).   
    - make it *probability* of succesful attack \* attack rate  
    - remove 'p'  
    - remove 'alpha' 
DONE - add predator fitness  
DONE - modify rate change for 3->2 to `a_in + attack_rate`. Make it a variable and put that into matrix.  

**Verbal description of fitness functions**  
Prey have a choice between staying in refuge or going into open. Inside the refuge, the prey have high survivorship, but low energy intake. In the open, the prey have high energy intake but low surviorship. Survivorship depends on if 1) the predator enters the patch while the prey are out 2) or if the prey leave the refuge when the predator is present.  

The predator can change between being among the patches, or in some "environment". The environment is an average energy intake. When in one of the three patches, the predator fitness depends on the proportion of time the prey spends in the open, and a specific capture rate if the prey happen to be in or going into the open. 


Operation:  

1. Assign movement rates (this will be a vector of rates when creating adaptive landscape)  
2. Determine proportion spent in each state  
3. Calculate fitness based on time spent in each state  

For function mu(), is N always 3? -> yes  
Does p change to b\_in out also? -> this is the proportion of b\_in/out from the distrib() function  
Does risk_rate only use dist[3]? -> No, it uses dist[1] and dist[3], where prey are in open


**Future dimensions**  
1. add more patches with variable rates  

2. add the ability for prey to acquire information that helps sense predation risk. eg, "peaking out"  

3. adding state changes when predator is or is not present  

4. adjust predation risk in refuge so that predator is able to attack there (albeit a low risk to prey) 