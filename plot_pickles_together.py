# -*- coding: utf-8 -*-
"""
Created on Thu Jul 14 16:12:15 2016

Branch: riskyRefuge

@author: PCain
"""

import pickle
import numpy as np
import matplotlib.pyplot as plt
from adapt_dyn import prop_time
from matplotlib.patches import Rectangle


#test = "environment"
test = "lethality"
N = 0

attack_rate = 1.0
numgen = 2000
environment1, environment2, environment3 = 0.6, 1.0, 3.0

number_points = 71       #For static prey
#number_points = 8       #For static pred
ymaximum = 35


#---------------------------------------------------------------------------------------------#
# These are graphs used in manuscript
u = 0.00
#
#pickleFile1 = "C:\Users\pcain\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen8000_2018-05-16_0.p"    #LowMOC    GAME
#pickleFile2 = "C:\Users\pcain\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen8000_2018-05-17_0.p"    #MedMOC    GAME
#pickleFile3 = "C:\Users\pcain\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen8000_2018-05-22_0.p"    #HighMOC   GAME

#pickleFile1 = "C:\Users\pcain\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen1000_2018-05-15_1.p"    #Slow   Static Predator
#pickleFile2 = "C:\Users\pcain\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen1000_2018-05-17_1.p"    #Med    Static Predator
#pickleFile3 = "C:\Users\pcain\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen1000_2018-05-17_0.p"    #Fast   Static Predator

pickleFile1 = "C:\Users\pcain\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen2000_2018-06-19_0.p"    #Slow   HIGH Environment
pickleFile2 = "C:\Users\pcain\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen2000_2019-06-06_1.p"    #Med    HIGH Environment
pickleFile3 = "C:\Users\pcain\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen2000_2018-06-18_1.p"    #Fast   HIGH Environment

#pickleFile1 = "C:\Users\pcain\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen2000_2018-06-20_2.p"    #Slow   MED Environment
#pickleFile2 = "C:\Users\pcain\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen2000_2019-06-05_0.p"    #Med    MED Environment
#pickleFile3 = "C:\Users\pcain\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen2000_2018-06-20_0.p"    #Fast   MED Environment

#pickleFile1 = "C:\Users\pcain\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen2000_2018-06-22_2.p"    #Slow  Low Environment
#pickleFile2 = "C:\Users\pcain\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen2000_2019-06-06_0.p"    #Med   Low Environment
#pickleFile3 = "C:\Users\pcain\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen2000_2018-06-22_1.p"    #Fast  Low Environment

#pickleFile1 = "C:\Users\pcain1\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen2000_2019-06-06_0.p"    #Low   
#pickleFile2 = "C:\Users\pcain1\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen2000_2019-06-05_0.p"    #Med    mid speeds
#pickleFile3 = "C:\Users\pcain1\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen2000_2019-06-06_1.p"    #High   

#pickleFile1 = "C:\Users\pcain1\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen2000_2018-06-22_1.p"    #Low   
#pickleFile2 = "C:\Users\pcain1\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen2000_2018-06-22_2.p"    #Med     test
#pickleFile3 = "C:\Users\pcain1\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen2000_2018-06-22_2.p"    #High   


#---------------------------------------------------------------------------------------------#

plt.close()
#plt.xkcd(scale=0.5, length=150, randomness=0.5)

widthline = 3
hfont = {'fontname':'monospace'}

points1, points2, points3 = pickle.load(open(pickleFile1,"rb")), pickle.load(open(pickleFile2,"rb")), pickle.load(open(pickleFile3,"rb"))

prop_time_points1, prop_time_points2, prop_time_points3 = [],[],[]

a_in_end1 = points1["a_in_endpoints"]
b_in_end1 = points1["b_in_endpoints"]
b_out_end1 = points1["b_out_endpoints"]
a_out_end1 = points1["a_out_endpoints"]
a_out_atk_end1 = points1["a_out_attack_endpoints"]
b_out_atk_end1 = points1["b_out_attack_endpoints"]

a_in_end2 = points2["a_in_endpoints"]
b_in_end2 = points2["b_in_endpoints"]
b_out_end2 = points2["b_out_endpoints"]
a_out_end2 = points2["a_out_endpoints"]
a_out_atk_end2 = points2["a_out_attack_endpoints"]
b_out_atk_end2 = points2["b_out_attack_endpoints"]
    

a_in_end3 = points3["a_in_endpoints"]
b_in_end3 = points3["b_in_endpoints"]
b_out_end3 = points3["b_out_endpoints"]
a_out_end3 = points3["a_out_endpoints"]
a_out_atk_end3 = points3["a_out_attack_endpoints"]
b_out_atk_end3 = points3["b_out_attack_endpoints"]
    
for i in range(0,number_points,1):

    prop_time_points1.append(prop_time(N, a_in_end1[i], a_out_end1[i], a_out_atk_end1[i], b_in_end1[i], b_out_end1[i], b_out_atk_end1[i], attack_rate))
    prop_time_points2.append(prop_time(N, a_in_end2[i], a_out_end2[i], a_out_atk_end2[i], b_in_end2[i], b_out_end2[i], b_out_atk_end2[i], attack_rate))
    prop_time_points3.append(prop_time(N, a_in_end3[i], a_out_end3[i], a_out_atk_end3[i], b_in_end3[i], b_out_end3[i], b_out_atk_end3[i], attack_rate))

length = np.arange(1.0, 8.1, 0.1)            #For static prey
#length = range(1,number_points+1,1)           #For static pred
#length = np.linspace(0.1, 0.8,8)

fig, ((ax1,ax2,ax3), (ax4,ax5,ax6), (ax7,ax8,ax9)) = plt.subplots(3,3, sharex = True, sharey = False, figsize = (20,15))
my_xticks = ['0.1','0.2','0.3','0.4','0.5','0.6','0.7','0.8']
plt.xticks(range(1,8+1,1), my_xticks)

ax1.plot(length, points1["b_in_endpoints"], "b:", linewidth = widthline, label = "Enter open (TAE)")
ax1.plot(length, points1["b_out_endpoints"], "k", linewidth = widthline, label = "Depart open (TAD)")
ax1.plot(length, points1["b_out_attack_endpoints"], "r--", linewidth = widthline, label = "Depart after attack (PAD)")
ax1.set_ylim(ymin=0, ymax=ymaximum)
ax1.set_xlabel("Low MOC", fontsize = 25)
ax1.xaxis.set_label_coords(0.5, 1.2)
ax1.set_ylabel("Rate of\nPredator Behavior", fontsize = 15, rotation = "vertical", **hfont)
ax1.minorticks_on()
ax1.tick_params('x', width=2, which='major')
ax1.tick_params('x', width=2, which='minor')

ax2.plot(length, points2["b_in_endpoints"], "b:", linewidth = widthline, label = "Enter open (TAE)")
ax2.plot(length, points2["b_out_endpoints"], "k", linewidth = widthline, label = "Depart open (TAD)")
ax2.plot(length, points2["b_out_attack_endpoints"], "r--", linewidth = widthline, label = "Depart after attack (PAD)")
ax2.legend(loc='center right', bbox_to_anchor=(3., 0.5), fontsize = "large", fancybox = True, frameon=False)
ax2.set_ylim(ymin=0, ymax=ymaximum)
ax2.set_xlabel("Medium MOC", fontsize = 25)
ax2.xaxis.set_label_coords(0.5, 1.2)
ax2.minorticks_on()
ax2.tick_params('x', width=2, which='major')
ax2.tick_params('x', width=2, which='minor')

ax3.plot(length, points3["b_in_endpoints"], "b:", linewidth = widthline, label = "Enter open (TAE)")
ax3.plot(length, points3["b_out_endpoints"], "k", linewidth = widthline, label = "Depart open (TAD)")
ax3.plot(length, points3["b_out_attack_endpoints"], "r--", linewidth = widthline, label = "Depart after attack (PAD)")
ax3.set_ylim(ymin=0, ymax=ymaximum)
ax3.set_xlabel("High MOC", fontsize = 25)
ax3.xaxis.set_label_coords(0.5, 1.2)
ax3.minorticks_on()
ax3.tick_params('x', width=2, which='major')
ax3.tick_params('x', width=2, which='minor')
#ax3.axvspan(3.0, 3.2, color = "gray", alpha = 0.8, lw=0)
#ax9.axvspan(0.4, 0.44, color = "gray", alpha = 0.8, lw=0)

ax4.plot(length, points1["a_in_endpoints"], "b:", linewidth = widthline, label = "Enter refuge (TAE)")
ax4.plot(length, points1["a_out_endpoints"], "k", linewidth = widthline, label = "Depart refuge (TAD)")
ax4.plot(length, points1["a_out_attack_endpoints"], "r--", linewidth = widthline, label = "Depart after attack (PAD)")
ax4.set_ylim(ymin=0, ymax=ymaximum)
ax4.set_ylabel("Rate of\nPrey Behavior", fontsize = 15, rotation = "vertical", **hfont)
ax4.minorticks_on()
ax4.tick_params('x', width=2, which='major')
ax4.tick_params('x', width=2, which='minor')

ax5.plot(length, points2["a_in_endpoints"], "b:", linewidth = widthline, label = "Enter refuge (TAE)")
ax5.plot(length, points2["a_out_endpoints"], "k", linewidth = widthline, label = "Depart refuge (TAD)")
ax5.plot(length, points2["a_out_attack_endpoints"], "r--", linewidth = widthline, label = "Depart after attack (PAD)")
ax5.legend(loc='center right', bbox_to_anchor=(3., 0.5), fontsize = "large", fancybox = True, frameon=False)
ax5.set_ylim(ymin=0, ymax=ymaximum)
ax5.set_xlabel("Lethality", fontsize = 30, **hfont)
ax5.xaxis.set_label_coords(0.5, -1.4)
ax5.minorticks_on()
ax5.tick_params('x', width=2, which='major')
ax5.tick_params('x', width=2, which='minor')

ax6.plot(length, points3["a_in_endpoints"], "b:", linewidth = widthline, label = "Enter refuge")
ax6.plot(length, points3["a_out_endpoints"], "k", linewidth = widthline, label = "Depart refuge (TAD)")
ax6.plot(length, points3["a_out_attack_endpoints"], "r--", linewidth = widthline, label = "Depart after attack")
ax6.set_ylim(ymin=0, ymax=ymaximum)
ax6.minorticks_on()
ax6.tick_params('x', width=2, which='major')
ax6.tick_params('x', width=2, which='minor')
#ax6.axvspan(0.4, 0.44, color = "gray", alpha = 0.8, lw=0)


state0_points1,state1_points1,state2_points1,state3_points1,state4_points1,state5_points1 = zip(*prop_time_points1)
state0_points2,state1_points2,state2_points2,state3_points2,state4_points2,state5_points2 = zip(*prop_time_points2)
state0_points3,state1_points3,state2_points3,state3_points3,state4_points3,state5_points3 = zip(*prop_time_points3)

y1 = np.row_stack((state0_points1,state1_points1,state4_points1,state2_points1,state3_points1,state5_points1)) 
y2 = np.row_stack((state0_points2,state1_points2,state4_points2,state2_points2,state3_points2,state5_points2))
y3 = np.row_stack((state0_points3,state1_points3,state4_points3,state2_points3,state3_points3,state5_points3))

ax7.stackplot(length, y1, colors = ('b','r','g','k','c','m'))
ax8.stackplot(length, y2, colors = ('b','r','g','k','c','m'))
ax9.stackplot(length, y3, colors = ('b','r','g','k','c','m'))

ax7.set_ylim([0,1])
ax7.set_xlim([1,8])     
ax8.set_ylim([0,1])
ax9.set_ylim([0,1])

#ax7.set_xlim([1,max(length)])     #maybe set the min/max xlimits to a variable? set_xlim([min(length),max(length)])?
ax7.set_ylabel("Proportion of time\nin each state", fontsize = 15, rotation = "vertical", **hfont)


p1 = Rectangle((0, 0), 1, 1, fc="b")
p2 = Rectangle((0, 0), 1, 1, fc="r")
p3 = Rectangle((0, 0), 1, 1, fc="g")
p4 = Rectangle((0, 0), 1, 1, fc="k")
p5 = Rectangle((0, 0), 1, 1, fc="c")
p6 = Rectangle((0, 0), 1, 1, fc="m")

plt.legend([p6, p5, p4, p3, p2, p1], ["Prey in Refuge (attack)/Pred in Open", "Prey in Open/Pred in Open", "Prey in Refuge/Pred in Open", "Prey in Refuge (attack)/Predator in Env","Prey in Open/Predator in Env","Prey in Refuge/Predator in Env"],
           bbox_to_anchor=(2.1,0.9), fontsize = "large", ncol=1, frameon=False)

    

plt.show()
