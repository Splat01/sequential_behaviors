# -*- coding: utf-8 -*-
"""
Modified on 10/28/2014
Now include k0list to allow different killing efficiencies in different patches

Includes chaninge the movement rule of prey. Now consider rate of movement to be a 
function of the number of prey in each habitat. This no longe assumes
a 'probability of following' (which has conceptual problems)
New function uses Ioannou et al (2***) function that for the rate of movement
between habitats as a function of the number of group members in each habitat.

rate*(n0**beta/n1**gamma)

rate is the base rate of movement, n0 is the number of members in other habitat
and n1 is the number of members in present habitat.

I am going to start with a simple model that only accounts for ratio

rate*(n0/n1)**gamma

test

@author: wmitchell
"""
import sys
import numpy as np
import scipy 
from scipy import linalg
from scipy.misc import derivative
import matplotlib
from matplotlib import pyplot as plt

def nrate(rate, n_here, n_other, c):
    """
    Uses number of conspecifics in each habitat to influence rate of movement
    between habitats. Includes the evolutionary parameter, c. If c is zero, 
    there is no effect of conspecific distribution. If c is positive, then
    individuals are attracted. If c is negative, then individuals are 
    repelled.
    """
    
    return rate*((.5+n_other)/(.5+n_here))**c
            

def matQ1(a,b,c):
    """
    0 - refuge
    1 - open
    
    """
    
    q01 = a[0]
    q00 = -q01
    
    q10 = b[0]
    q11 = -q10
    Q = np.matrix([[q00, q01],[q10,q11]], dtype='float')
    return Q
    
    
def matQ2(a,b,c):
    """
    Received the lists of movement rates and association parameters and returns
    the transition rate matrix among the four states listed below. The list, a,
    has two elements, a[i] is the rate of movement from refuge to open for 
    individual i. b[i] is similarly represents the movement from open to refuge.
    
    0 - refuge: 0,1       open: none
    1 - refuge: 0         open: 1
    2 - refuge: 1         open: 0
    3 - refuge: none      open: 0,1
    
    """
    
    q01 = nrate(a[1],1,0,c[1])          #a[1]*(1-c[0])
    q02 = nrate(a[0],1,0,c[0])              #a[0]*(1-c[1])
    q03 = 0.                        #a[0]*c[1] + a[1]*c[0]
    q00 = -q01-q02-q03
    
    q10 = nrate(b[1],0,1,c[1])
    q12 = 0.         
    q13 = nrate(a[0],0,1,c[0])      
    q11 = -q10-q12-q13
    
    q20 = nrate(b[0],0,1,c[0])
    q21 = 0
    q23 = nrate(a[1],0,1,c[1])
    q22 = -q20-q21-q23
    
    q30 = 0.                        #b[0]*c[1] + b[1]*c[0]
    q31 = nrate(b[0],1,0,c[0]) 
    q32 = nrate(b[1],1,0,c[1])
    q33 = -q30-q31-q32
    
    Q = np.matrix([[q00, q01, q02, q03], 
                   [q10, q11, q12, q13],
                   [q20, q21, q22, q23], 
                   [q30, q31, q32, q33]], dtype='float')
    
    return Q
    
    
def matQ3(a,b,c):
    """
    0 - refuge: 0,1,2       open: none
    1 - refuge: 0,1         open: 2
    2 - refuge: 0,2         open: 1
    3 - refuge: 1,2         open: 0
    4 - refuge: 0           open: 1,2
    5 - refuge: 1           open: 0,2
    6 - refuge: 2           open: 0,1
    7 - refuge: none        open: 0,1,2
    
    """
    
    q01 = nrate(a[2],2,0,c[2])          #a[2]*(1-c[0])*(1-c[1])
    q02 = nrate(a[1],2,0,c[1])
    q03 = nrate(a[0],2,0,c[0])
    q04 = 0.
    q05 = 0.
    q06 = 0.
    q07 = 0.
    q00 = -q01-q02-q03-q04-q05-q06-q07
    
    q10 = nrate(b[2],0,2,c[2])
    q12 = 0         
    q13 = 0
    q14 = nrate(a[1],1,1,c[1])
    q15 = nrate(a[0],1,1,c[0])
    q16 = 0
    q17 = 0.
    q11 = -q10-q12-q13-q14-q15-q16-q17
    
    q20 = nrate(b[1],0,2,c[1])
    q21 = 0
    q23 = 0
    q24 = nrate(a[2],1,1,c[2])
    q25 = 0
    q26 = nrate(a[0],1,1,c[0])
    q27 = 0.
    q22 = -q20-q21-q23-q24-q25-q26-q27
    
    q30 = nrate(b[0],0,2,c[0])
    q31 = 0
    q32 = 0
    q34 = 0
    q35 = nrate(a[2],1,1,c[2])
    q36 = nrate(a[1],1,1,c[1])
    q37 = 0.
    q33 = -q30-q31-q32-q34-q35-q36-q37
    
    q40 = 0.
    q41 = nrate(b[1],1,1,c[1])
    q42 = nrate(b[2],1,1,c[2])
    q43 = 0
    q45 = 0
    q46 = 0
    q47 = nrate(a[0],0,2,c[0])
    q44 = -q40-q41-q42-q43-q45-q46-q47
    
    q50 = 0.
    q51 = nrate(b[0],1,1,c[0])
    q52 = 0.
    q53 = nrate(b[2],1,1,c[2])
    q54 = 0
    q56 = 0
    q57 = nrate(a[1],0,2,c[1])
    q55 = -q50-q51-q52-q53-q54-q56-q57
    
    q60 = 0.
    q61 = 0.
    q62 = nrate(b[0],1,1,c[0])
    q63 = nrate(b[1],1,1,c[0])
    q64 = 0.
    q65 = 0.
    q67 = nrate(a[2],0,2,c[2])
    q66 = -q60-q61-q62-q63-q64-q65-q67
    
    q70 = 0.
    q71 = 0.
    q72 = 0.
    q73 = 0.
    q74 = nrate(b[0],2,0,c[0])
    q75 = nrate(b[1],2,0,c[1])
    q76 = nrate(b[2],2,0,c[2])
    q77 = -q70-q71-q72-q73-q74-q75-q76
    
    Q = np.matrix([[q00,q01,q02,q03,q04,q05,q06,q07],
                   [q10,q11,q12,q13,q14,q15,q16,q17],
                    [q20,q21,q22,q23,q24,q25,q26,q27],
                    [q30,q31,q32,q33,q34,q35,q36,q37],
                    [q40,q41,q42,q43,q44,q45,q46,q47],
                    [q50,q51,q52,q53,q54,q55,q56,q57],
                    [q60,q61,q62,q63,q64,q65,q66,q67],
                    [q70,q71,q72,q73,q74,q75,q76,q77]], dtype='float')
    return Q

def mu(N,p,k0,k1,eta0,beta0,alpha):
    """
    per capita rate of predation risk.
    Argument
        N:      number of exposed prey in group
        p:      proportion of time predator hunts in patch
        k0:     base probability of predator killing a prey
        alpha:  area of search 
        k1:     effect of N on prob that predator kills prey during attack
        eta0:   effect of N on probability that predator detects group
        beta0:  probability an individual detect the predator
    Returns a float
    """
    
#    eta = 1. - np.exp(-eta0*N)          # rate of detection of group of N prey during scan
#    eta = 1. - eta0**N
    eta1 = 0.5                           #magic number
    eta = 1. - eta1**(1. + eta0*(N-1.))
#    eta = 1. - 0.5**(1.+eta0*N)
    beta = (1-beta0)**N                  # probability that no prey detects predator
    #k = k0*(N/(k1 + N))                 # prob that pred kills one prey from group [k1=0 is perfect pred dilution]
    k = (1.-(1.-k0)**(1.+k1*(N-1.)))
    
#    if N == 0:
#        return 0
#    else:
    
    return p*alpha*eta*beta*k/N
    
    
def distrib(Q):
    """
    Takes transition rate matrix, Q, and returns distribution among states.
    """
    evals,evecs = scipy.linalg.eig(Q,left=True,right=False)
    index = np.argmin(evals**2)
    return evecs[:,index]/sum(evecs[:,index])

def fitness2(a,b,c,p,N,k0,k1,eta0,beta0,alpha,comp):
    """
    Calculates fitness of first-indexed individual (usually 'i') in a
 
    """
    food = 1.
    init_state = 0.0
    Q = matQ2(a,b,c)
    dist = distrib(Q)
    intake = (dist[2]+dist[3]/(1.+comp*1.))*food
    risk_rate = dist[2]*mu(1,p,k0,k1,eta0,beta0,alpha) + \
                dist[3]*mu(2,p,k0,k1,eta0,beta0,alpha)
                
    return (intake+init_state)*np.exp(-risk_rate)

    
def fitness3(a,b,c,p,N,k0,k1,eta0,beta0,alpha):
    """
    calculate fitness of individualindexed by 0
    """
    food = 1.
    init_state = 0.0
    Q = matQ3(a,b,c)
    dist = distrib(Q)
    intake = (dist[3]+dist[5]+dist[6]+dist[7])*food
    risk_rate = dist[3]*mu(1,p,k0,k1,eta0,beta0,alpha) + \
                dist[5]*mu(2,p,k0,k1,eta0,beta0,alpha) + \
                dist[6]*mu(2,p,k0,k1,eta0,beta0,alpha) + \
                dist[7]*mu(3,p,k0,k1,eta0,beta0,alpha)
                
    return (intake+init_state)*np.exp(-risk_rate)
    
#def fitness(a,b,c,p,N,k0,k1,eta0,beta0,alpha,comp):
#    """
#    Calculates prey fitness as a function of movement rates to and from refuge (a,b),
#    association parameter (c), predator behavior (p), killing efficiency (pars k0,k1))
#    """
#    
#    food = 1.
#    init_state = 0.0
#    
#    if N==1:
#        Q = matQ1(a,b,c)
#        dist = distrib(Q)
#        intake = dist[1]*food
#        risk_rate = dist[1]*mu(1,p,k0,k1,eta0,beta0,alpha)
#
#    if N==2:
#        Q = matQ2(a,b,c)
#        dist = distrib(Q)
#        intake = (dist[2]+dist[3]/(1.+comp*1.))*food
#        risk_rate = dist[2]*mu(1,p,k0,k1,eta0,beta0,alpha) + \
#                    dist[3]*mu(2,p,k0,k1,eta0,beta0,alpha)
#     
#    elif N==3:
#        Q = matQ3(a,b,c)
#        dist = distrib(Q)
#        intake = (dist[3]+(dist[5]+dist[6])/(1.+comp*1.)+dist[7]/(1.+comp*2.))*food
#        risk_rate = dist[3]*mu(1,p,k0,k1,eta0,beta0,alpha) + \
#                    dist[5]*mu(2,p,k0,k1,eta0,beta0,alpha) + \
#                    dist[6]*mu(2,p,k0,k1,eta0,beta0,alpha) + \
#                    dist[7]*mu(3,p,k0,k1,eta0,beta0,alpha)
#                
#    return (intake+init_state)*np.exp(-risk_rate)
        
    
def prey_fitness(a,b,c,p,N,k0,k1,eta0,beta0,alpha,food,comp):
    """
    Calculates prey fitness as a function of movement rates to and from refuge (a,b),
    association parameter (c), predator behavior (p), killing efficiency (pars k0,k1))
    """
    
#    food  
    init_state = 0.2
    
    if N==1:
        Q = matQ1(a,b,c)
        dist = distrib(Q)
        intake = dist[1]*food
        risk_rate = dist[1]*mu(1,p,k0,k1,eta0,beta0,alpha)

    if N==2:
        Q = matQ2(a,b,c)
        dist = distrib(Q)
        intake = (dist[2]+dist[3]/(1.+comp*1.))*food
        risk_rate = dist[2]*mu(1,p,k0,k1,eta0,beta0,alpha) + \
                    dist[3]*mu(2,p,k0,k1,eta0,beta0,alpha)
     
    elif N==3:
        Q = matQ3(a,b,c)
        dist = distrib(Q)
        intake = (dist[3]+(dist[5]+dist[6])/(1.+comp*1.)+dist[7]/(1.+comp*2.))*food
        risk_rate = dist[3]*mu(1,p,k0,k1,eta0,beta0,alpha) + \
                    dist[5]*mu(2,p,k0,k1,eta0,beta0,alpha) + \
                    dist[6]*mu(2,p,k0,k1,eta0,beta0,alpha) + \
                    dist[7]*mu(3,p,k0,k1,eta0,beta0,alpha)
                
    return (intake+init_state)*np.exp(-risk_rate)

    
def predfit32k0(p,a1,b1,c1,a2,b2,c2,N1,N2,k0list,k1,eta0,beta0,alpha):
    """
    Modified to take k0list
    Describes predator fitness as a function of individual predator only; does
    not account for 'resident' predator strategy, and the impact that may have
    on the prey behaviors.
    """
    T = 1.          # time period
#    moc = 2.5      # missed opportunity cost of hunting in patch (a rate of energy)
    
    k0_1 = k0list[0]
    if N1==1:
        Q1 = matQ1(a1,b1,c1)
        dist1 = distrib(Q1)
        rate1 = dist1[1]*mu(1,p,k0_1,k1,eta0,beta0,alpha)
        
    if N1==2:
        Q1 = matQ2(a1,b1,c1)
        dist1 = distrib(Q1)
        rate1 =   dist1[1]*mu(1,p,k0_1,k1,eta0,beta0,alpha) \
                + dist1[2]*mu(1,p,k0_1,k1,eta0,beta0,alpha) \
                + dist1[3]*mu(2,p,k0_1,k1,eta0,beta0,alpha)*2 
    
    if N1==3:
        
        Q1 = matQ3(a1,b1,c1)
        dist1 = distrib(Q1)
        rate1 =   dist1[1]*mu(1,p,k0_1,k1,eta0,beta0,alpha) \
                + dist1[2]*mu(1,p,k0_1,k1,eta0,beta0,alpha) \
                + dist1[3]*mu(1,p,k0_1,k1,eta0,beta0,alpha) \
                + dist1[4]*mu(2,p,k0_1,k1,eta0,beta0,alpha)*2 \
                + dist1[5]*mu(2,p,k0_1,k1,eta0,beta0,alpha)*2 \
                + dist1[6]*mu(2,p,k0_1,k1,eta0,beta0,alpha)*2 \
                + dist1[7]*mu(3,p,k0_1,k1,eta0,beta0,alpha)*3
    
    k0 = k0list[1]
    if N2==1: 
        
        Q1 = matQ1(a1,b1,c1)
        dist2 = distrib(Q1)
        rate2 = dist2[1]*mu(1,p,k0,k1,eta0,beta0,alpha)
        
        
    elif N2==2: 
        
        Q2 = matQ2(a2,b2,c2)
        dist2 = distrib(Q2)
        rate2 =   dist2[1]*mu(1,1-p,k0,k1,eta0,beta0,alpha) \
                + dist2[2]*mu(1,1-p,k0,k1,eta0,beta0,alpha) \
                + dist2[3]*mu(2,1-p,k0,k1,eta0,beta0,alpha)*2 
                               


    return rate1 + rate2
    

    
def predfit3patch(p1,p2,a1,b1,c1,a2,b2,c2,a3,b3,c3,N1,N2,N3,k0list,k1,eta0,beta0,alpha):
    """
    Predator forages in three patches (Modified to take k0list).
    Describes predator fitness as a function of individual predator only; does
    not account for 'resident' predator strategy, and the impact that may have
    on the prey behaviors.
    """
    T = 1.          # time period
#    moc = 2.5      # missed opportunity cost of hunting in patch (a rate of energy)
    
    k0 = k0list[0]
    if N1==1:
        Q1 = matQ1(a1,b1,c1)
        dist1 = distrib(Q1)
        rate1 = dist1[1]*mu(1,p,k0,k1,eta0,beta0,alpha)
        
    if N1==2:
        Q1 = matQ2(a1,b1,c1)
        dist1 = distrib(Q1)
        rate1 =   dist1[1]*mu(1,p,k0,k1,eta0,beta0,alpha) \
                + dist1[2]*mu(1,p,k0,k1,eta0,beta0,alpha) \
                + dist1[3]*mu(2,p,k0,k1,eta0,beta0,alpha)*2 
    
    if N1==3:
        
        Q1 = matQ3(a1,b1,c1)
        dist1 = distrib(Q1)
        rate1 =   dist1[1]*mu(1,p,k0,k1,eta0,beta0,alpha) \
                + dist1[2]*mu(1,p,k0,k1,eta0,beta0,alpha) \
                + dist1[3]*mu(1,p,k0,k1,eta0,beta0,alpha) \
                + dist1[4]*mu(2,p,k0,k1,eta0,beta0,alpha)*2 \
                + dist1[5]*mu(2,p,k0,k1,eta0,beta0,alpha)*2 \
                + dist1[6]*mu(2,p,k0,k1,eta0,beta0,alpha)*2 \
                + dist1[7]*mu(3,p,k0,k1,eta0,beta0,alpha)*3
    
    k0 = k0list[1]
    if N2==1: 
        
        Q1 = matQ1(a1,b1,c1)
        dist2 = distrib(Q1)
        rate2 = dist2[1]*mu(1,p1,k0,k1,eta0,beta0,alpha)
        
        
    if N2==2: 
        
        Q2 = matQ2(a2,b2,c2)
        dist2 = distrib(Q2)
        rate2 =   dist2[1]*mu(1,p2,k0,k1,eta0,beta0,alpha) \
                + dist2[2]*mu(1,p2,k0,k1,eta0,beta0,alpha) \
                + dist2[3]*mu(2,p2,k0,k1,eta0,beta0,alpha)*2 
                               
                               
    k0 = k0list[2]
    if N3==1: 
        
        Q1 = matQ1(a3,b3,c3)
        dist3 = distrib(Q1)
        rate3 = dist3[1]*mu(1,p,k0,k1,eta0,beta0,alpha)
        
        
    elif N3==2: 
        
        Q2 = matQ2(a3,b3,c3)
        dist3 = distrib(Q2)
        rate3 =   dist3[1]*mu(1,1-p1-p2,k0,k1,eta0,beta0,alpha) \
                + dist3[2]*mu(1,1-p1-p2,k0,k1,eta0,beta0,alpha) \
                + dist3[3]*mu(2,1-p1-p2,k0,k1,eta0,beta0,alpha)*2 


    return rate1 + rate2 + rate3
    
