# -*- coding: utf-8 -*-
"""
Created on Mon Nov  2 15:09:39 2015

Branch: riskyRefuge

@author: PCain
"""

import pickle
import numpy as np
import matplotlib.pyplot as plt

#test = "environment"
test = "lethality"


pickleFile = "C:\Users\pcain1\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen8000_2018-05-23_1.p"         #Temp File
attack_rate = 1.0
environment = 1.4
lethality = 99.0
numgen = 8000
u = 0.0

length = np.arange(1.0, 9.0, 1)

ymaximum = 40
#Endpoints_lethality_numgen8000_2018-05-22_0.p  #3.0env
#Endpoints_lethality_numgen8000_2018-05-22_1.p  #1.5env

#pickleFile = "Endpoints_lethality_numgen12000_2016-07-14_0.p"         #Temp File
#attack_rate = 1.0
#environment = 0.2
#lethality = 0.6
#numgen = 12000
#u = 0.1

#pickleFile = "Endpoints_lethality_numgen12000_2016-07-11_0.p"         #Temp File
#attack_rate = 1.0
#environment = 0.3
#lethality = 0.6
#numgen = 12000
#u = 0.1

#-----------------------------------------#
#pickleFile = "Endpoints_lethality_numgen12000_2016-06-23_1.p"         #Temp File
#attack_rate = 1.0
#environment = 0.1
#lethality = 0.6
#numgen = 12000
#u = 0.05

#pickleFile = "Endpoints_lethality_numgen12000_2016-06-24_0.p"         #Temp File
#attack_rate = 1.0
#environment = 0.2
#lethality = 0.6
#numgen = 12000
#u = 0.05

#pickleFile = "Endpoints_lethality_numgen12000_2016-07-01_0.p"         #Temp File
#attack_rate = 1.0
#environment = 0.3
#lethality = 0.6
#numgen = 12000
#u = 0.05

#-----------------------------------------#
#pickleFile = "Endpoints_lethality_numgen12000_2016-06-23_1.p"         #Temp File
#attack_rate = 1.0
#environment = 0.1
#lethality = 0.6
#numgen = 12000
#u = 0.01

#pickleFile = "Endpoints_lethality_numgen12000_2016-06-24_0.p"         #Temp File
#attack_rate = 1.0
#environment = 0.2
#lethality = 0.6
#numgen = 12000
#u = 0.01

#pickleFile = "Endpoints_lethality_numgen12000_2016-06-22_0.p"         #Temp File
#attack_rate = 1.0
#environment = 0.3
#lethality = 0.6
#numgen = 12000
#u = 0.01

#-----------------------------------------#
#pickleFile = "Endpoints_successful_attack_numgen12000_2015-12-10.p"        #Low environment
#attack_rate = 1.0
#environment = 0.1
#lethality = 0.6
#numgen = 12000
#u = 0.00

#pickleFile = "Endpoints_successful_attack_numgen2000_2015-12-07.p"         #Med environment
#attack_rate = 1.0
#environment = 0.2
#lethality = 0.6
#numgen = 12000
#u = 0.00

#pickleFile = "Endpoints_successful_attack_numgen12000_2015-12-08.p"        #High Environment
#attack_rate = 1.0
#environment = 0.3
#lethality = 0.6
#numgen = 12000
#u = 0.00


plt.close()
#plt.xkcd(scale=0.5, length=150, randomness=0.5)

widthline = 5
points = pickle.load(open(pickleFile,"rb"))

#length = range(length("a_in_endpoints"))
#length = np.linspace(3.3, 3.7, 5)


#ymaximum = max(points["b_out_attack_endpoints"] + points["b_out_endpoints"] + points["b_in_endpoints"] + points["a_out_attack_endpoints"] + points["a_out_endpoints"] + points["a_in_endpoints"]) * 1.1     

fig, (ax1,ax2) = plt.subplots(2,1, sharex = True, sharey = False, figsize = (8,15))

if test == "lethality":
    #for Succesful Attack runs
    fig.text(0.51,0.025, "Lethality", ha = "center", fontsize = 40)
    plt.suptitle("| numgen = %3.0f | attack_rate = %3.1f | environment = %3.2f | riskyRefuge = %3.2f" 
                    % (numgen, attack_rate, environment, u))
else:
    #for environment runs
    fig.text(0.51,0.05, "Environment", ha = "center", fontsize = 20)
    plt.suptitle("| numgen = %3.0f | attack_rate = %3.1f | lethality = %3.1f | riskyRefuge = %3.2f" 
                    % (numgen, attack_rate, lethality, u))
                    
#fig.text(0.5,0.02, pickleFile, ha = "center", fontsize = 10)
#fig.text(0.05,0.5, "Rate", ha = "center", fontsize = 40, rotation = "vertical")

ax1.plot(length, points["b_in_endpoints"], "b:", linewidth = widthline, label = "Enter open")
ax1.plot(length, points["b_out_endpoints"], "k", linewidth = widthline, label = "Depart open")
ax1.plot(length, points["b_out_attack_endpoints"], "r--", linewidth = widthline, label = "Depart_attack")
ax1.set_ylim(ymin=0, ymax=ymaximum)
ax1.set_xlim(xmin=1, xmax=8)
#ax1.set_title("Predator", fontsize = 40)
ax1.legend(loc = 0, fontsize = "small", title = "Predator", fancybox = True)

ax2.plot(length, points["a_in_endpoints"], "b:", linewidth = widthline, label = "Enter refuge")
ax2.plot(length, points["a_out_endpoints"], "k", linewidth = widthline, label = "Depart refuge")
ax2.plot(length, points["a_out_attack_endpoints"], "r--", linewidth = widthline, label = "Depart_attack")
ax2.set_ylim(ymin=0, ymax=ymaximum)
#ax2.set_title("Prey", fontsize = 40)
ax2.legend(loc = 0, title = "Prey", fontsize = "small", fancybox = True)

plt.show()