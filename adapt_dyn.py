# -*- coding: utf-8 -*-
"""
Created on Thu Feb 26 15:48:25 2015

Branch: riskyRefuge

@author: William Mitchell and Pat Cain

------

For N resident prey (N resident + 1 mutant) and 1 predator:
States are:

#    Prey            Pred
---  ------          ------
0    refuge          env
1    open            env
2    refuge          open
3    open            open
4    refuge(attack)  env
5    refuge(attack)  open

Assumptions: 
    -prey movement from one state to another is the sum of rates for all individuals
    -Rates of changing to same state 
    -A[3,1] won't happen so it's set to 0 (pred won't leave a patch when prey in open)
    -Has a number of residents (set to N) and mutant (1)

a = prey, b = predator

out = refuge to open (prey) -or- patch to environment (predator, in absence of prey in open)
in = open to refuge (prey) -or- environment to patch (predator)

Thus, state changes can be <a_out> or <b_in>

* Players can't move simultaneously (eg, both a_in and b_out), so states where 
this occurs are given rates of 0 (eg, [0,3], [1,2], [2,1], [3,0]). 

* The transition rate for 3 -> 1 (predator leaving to environment when prey
are in open) will not happen, so it is given a rate of 0. 

rxx = rate when both prey and pred change to identical state (a_in to a_in)

State change matrix (for reference)
[a00,a01,a02,a03,a04,a05]
[a10,a11,a12,a13,a14,a15]
[a20,a21,a22,a23,a24,a25]
[a30,a31,a32,a33,a34,a35]
[a40,a41,a42,a43,a44,a45]
[a50,a51,a52,a53,a54,a55]    
"""
import pickle
import numpy as np
import matplotlib.pyplot as plt
from groupfuncs05 import distrib    #distrib() takes transition rate matrix, Q, and returns distribution among states.
import time
import datetime
from os.path import isfile


def prey_fitness(N, a_in, a_out, a_in_r, a_out_r, a_out_attack, b_in_r, b_out_r, b_out_attack, attack_rate, kill_rate, u, comp, init_state, food, prey_move_cost):
    """
    Calculates prey fitness
    -variables intake and risk_rate use dist[1] and dist[3], for when prey is in open
    u is risk in refuge
    Includes state change penalty
    
    """
    
    Q = rate_matrix(N, a_in, a_out, a_in_r, a_out_r, a_out_attack, b_in_r, b_out_r, b_out_attack, attack_rate)
    dist = distrib(Q)
    intake = (dist[1] + dist[3] / (1. + comp * N))*food
    risk_rate_open = dist[3]*kill_rate 
    risk_rate_refuge = dist[2]*kill_rate*u
    penalty = (a_in * (dist[1]+dist[3]) + a_out * (dist[0]+dist[2]) + a_out_attack * (dist[4]+dist[5])) * prey_move_cost
    
#    print np.exp(-(risk_rate_open + risk_rate_refuge))
    return max((intake+init_state-penalty)*np.exp(-(risk_rate_open + risk_rate_refuge)), 0)
        
def pred_fitness_output(N, a_in, a_out, a_in_r, a_out_r, a_out_attack, b_in_r, b_out_r, b_out_attack, attack_rate, kill_rate, u, comp, init_state, food, prey_move_cost):
    Q = rate_matrix(N, a_in, a_out, a_in_r, a_out_r, a_out_attack, b_in_r, b_out_r, b_out_attack, attack_rate)
    dist = distrib(Q)
    risk_rate_open = dist[3]*kill_rate 
    risk_rate_refuge = dist[2]*kill_rate*u
    
    return np.exp(-(risk_rate_open + risk_rate_refuge))

def pred_fitness(N, a_in_r, a_out_r, a_out_attack, b_in_r, b_out_r, b_out_attack, environment, attack_rate, kill_rate, u, pred_move_cost):
    ''' 
    Calculates pred fitness versus residents and from environment
    Also includes state change penalty
    '''
    Q = rate_matrix(N, a_in_r, a_out_r, a_in_r, a_out_r, a_out_attack, b_in_r, b_out_r, b_out_attack, attack_rate)
    dist = distrib(Q)
    ratePatch = dist[3]*kill_rate 
    rateRefuge = dist[2]*kill_rate*u
    rateEnv = dist[0]*environment + dist[1]*environment + dist[4]*environment
    penalty = (b_in_r * (dist[0]+dist[1]+dist[4]) + b_out_r * dist[2] + b_out_attack * dist[5]) * pred_move_cost

    return ratePatch + rateRefuge + rateEnv - penalty
    
def pred_fitness_ratePatch(N, a_in_r, a_out_r, a_out_attack, b_in_r, b_out_r, b_out_attack, environment, attack_rate, kill_rate, u):
    """
    graphs energy return from being in patch vs time
    """
    Q = rate_matrix(N, a_in_r, a_out_r, a_in_r, a_out_r, a_out_attack, b_in_r, b_out_r, b_out_attack, attack_rate)
    dist = distrib(Q)
    ratePatch = dist[3] * kill_rate
    rateRefuge = dist[2] * kill_rate * u

    return ratePatch + rateRefuge 
    
def rate_matrix(N, a_in, a_out, a_in_r, a_out_r, a_out_attack, b_in_r, b_out_r, b_out_attack, attack_rate):
    '''
    Returns a vector of rates of change among states
    
    #    Prey            Pred
   ---  ------          ------
    0    refuge          env
    1    open            env
    2    refuge          open
    3    open            open
    4    refuge(attack)  env
    5    refuge(attack)  open
        
    '''    
   
    r00 = -a_out_r*N - a_out - b_in_r - 0  
    r01 = a_out_r*N + a_out
    r02 = b_in_r 
    r03 = 0
    r04 = 0
    r05 = 0
    
    r10 = a_in_r*N + a_in
    r11 = -a_in_r*N - a_in - 0 - b_in_r
    r12 = 0
    r13 = b_in_r
    r14 = 0
    r15 = 0
    
    r20 = b_out_r
    r21 = 0
    r22 = -b_out_r - 0 - a_out_r*N - a_out
    r23 = a_out_r*N + a_out
    r24 = 0
    r25 = 0
    
    r30 = 0
    r31 = 0
    r32 = a_in_r*N + a_in 
    r33 = -a_in_r*N - a_in - attack_rate
    r34 = 0
    r35 = attack_rate
    
    r40 = 0
    r41 = a_out_attack*N + a_out_attack
    r42 = 0
    r43 = 0
    r44 = -a_out_attack*N - a_out_attack - b_in_r 
    r45 = b_in_r
    
    r50 = 0
    r51 = 0
    r52 = 0
    r53 = a_out_attack*N + a_out_attack
    r54 = b_out_attack
    r55 = -a_out_attack*N - a_out_attack - b_out_attack  
    
    return np.matrix([[r00, r01, r02, r03, r04, r05],\
                      [r10, r11, r12, r13, r14, r15],\
                      [r20, r21, r22, r23, r24, r25],\
                      [r30, r31, r32, r33, r34, r35],\
                      [r40, r41, r42, r43, r44, r45],\
                      [r50, r51, r52, r53, r54, r55]], dtype='float')
        
def prop_time(N, a_in_r, a_out_r, a_out_attack, b_in_r, b_out_r, b_out_attack, attack_rate):
    "used to calculate proportion of time spent in each state"        
    Q = rate_matrix(N, a_in_r, a_out_r, a_in_r, a_out_r, a_out_attack, b_in_r, b_out_r, b_out_attack, attack_rate)
    dist = distrib(Q)        
    return dist

def predpreydyn(N, a_in_r, a_out_r, a_out_attack, b_in_r, b_out_r, b_out_attack, k0, k1, eta0, beta0, alpha, comp, init_state,
                food, environment, list_size, speed, numgen, attack_rate, lethality, kill_rate, u, prey_move_cost, pred_move_cost, updatePrey, updatePred, rateCapMax_predOutAttack):
        
    """----adaptive dynamics----"""
    generations = np.arange(0, numgen)
    
    # Empty lists for graphs
    a_in_graph, a_out_graph, a_out_attack_graph = [], [], []
    b_in_graph, b_out_graph, b_out_attack_graph = [], [], []
    prop_time_graph = []
    ratePatch_graph = []
    
    for gen in generations:
        a_in_graph.append(a_in_r)
        a_out_graph.append(a_out_r)
        a_out_attack_graph.append(a_out_attack)
        
        b_in_graph.append(b_in_r)
        b_out_graph.append(b_out_r)
        b_out_attack_graph.append(b_out_attack)
        
        # used for graphs of a_out/b_in AND return-rate of predator in patch
        prop_time_graph.append(prop_time(N, a_in_r, a_out_r, a_out_attack, b_in_r, b_out_r, b_out_attack, attack_rate))
        ratePatch_graph.append(pred_fitness_ratePatch(N, a_in_r, a_out_r, a_out_attack, b_in_r, b_out_r, b_out_attack, environment, attack_rate, kill_rate, u))

        #------a_in------#
        # for mutant values, make the list centered on resident value
        a_in_list = np.linspace(a_in_r - 0.5, a_in_r + 0.5, list_size)
        
        # calculate 'fitness' for each value of mutant phenotype
        a_in_fit = [prey_fitness(N, x, a_out_r, a_in_r, a_out_r, a_out_attack, b_in_r, b_out_r, b_out_attack, attack_rate, kill_rate, u, comp, init_state, food, prey_move_cost) for x in a_in_list]

        # generate slope, but have to convert xfit to numpy array for use in gradient function
        a_in_grad = np.array(np.gradient(a_in_fit))
        a_in_delt = a_in_grad[list_size/2]
        
        #------a_out------#    
        a_out_list = np.linspace(a_out_r - 0.5, a_out_r + 0.5, list_size)
        a_out_fit = [prey_fitness(N, a_in_r, x, a_in_r, a_out_r, a_out_attack, b_in_r, b_out_r, b_out_attack, attack_rate, kill_rate, u, comp, init_state, food, prey_move_cost) for x in a_out_list]
        
        a_out_grad = np.array(np.gradient(a_out_fit))
        a_out_delt = a_out_grad[list_size/2]   
        
        #------a_out_attack------#
        a_out_attack_list = np.linspace(a_out_attack - 0.5, a_out_attack + 0.5, list_size)
        a_out_attack_fit = [prey_fitness(N, a_in_r, a_out_r, a_in_r, a_out_r, x, b_in_r, b_out_r, b_out_attack, attack_rate, kill_rate, u, comp, init_state, food, prey_move_cost) for x in a_out_attack_list]
                
        a_out_attack_grad = np.array(np.gradient(a_out_attack_fit))
        a_out_attack_delt = a_out_attack_grad[list_size/2]
        
        #------b_in------#     
        b_in_list = np.linspace(b_in_r - 0.5, b_in_r + 0.5, list_size)
        b_in_fit = [pred_fitness(N, a_in_r, a_out_r, a_out_attack, x, b_out_r, b_out_attack, environment, attack_rate, kill_rate, u, pred_move_cost) for x in b_in_list]
        
        b_in_grad = np.array(np.gradient(b_in_fit))
        b_in_delt = b_in_grad[list_size/2]  
        
        #------b_out------#     
        b_out_list = np.linspace(b_out_r - 0.5, b_out_r + 0.5, list_size)
        b_out_fit = [pred_fitness(N, a_in_r, a_out_r, a_out_attack, b_in_r, x, b_out_attack, environment, attack_rate, kill_rate, u, pred_move_cost) for x in b_out_list]
        
        b_out_grad = np.array(np.gradient(b_out_fit))
        b_out_delt = b_out_grad[list_size/2]
        
        #------b_out_attack------#     
        b_out_attack_list = np.linspace(b_out_attack - 0.5, b_out_attack + 0.5, list_size)
        b_out_attack_fit = [pred_fitness(N, a_in_r, a_out_r, a_out_attack, b_in_r, b_out_r, x, environment, attack_rate, kill_rate, u, pred_move_cost) for x in b_out_attack_list]
        
        b_out_attack_grad = np.array(np.gradient(b_out_attack_fit))
        b_out_attack_delt = b_out_attack_grad[list_size/2]
        
        # update the resident value according to the slope of the function
        # evaluated at the resident value and the 'speed' parameter
            
        if updatePrey == True:
            
            a_in_r += a_in_delt*speed
            a_in_r = np.max([0, a_in_r])    
         
            a_out_r += a_out_delt*speed
            a_out_r = np.max([0, a_out_r])
            
            a_out_attack += a_out_attack_delt*speed
            a_out_attack = np.max([0, a_out_attack])
        
        if updatePred == True:
           
            b_in_r += b_in_delt*speed
#            b_in_r = np.max([0, b_in_r)                #no rate cap
            b_in_r = np.clip(b_in_r, 0, rateCapMax_predOutAttack)
            
            b_out_r += b_out_delt*speed
#            b_out_r = np.max([0, b_out_r])             #no rate cap
            b_out_r = np.clip(b_out_r, 0, rateCapMax_predOutAttack)

            b_out_attack += b_out_attack_delt*speed
#            b_out_attack = np.max([0, b_out_attack])   #no rate cap
            b_out_attack = np.clip(b_out_attack, 0, rateCapMax_predOutAttack)
        #--------------------------#
#        print pred_fitness_output(N, a_in_r, a_out_r, a_in_r, a_out_r, a_out_attack, b_in_r, b_out_r, b_out_attack, attack_rate, kill_rate, u, comp, init_state, food, prey_move_cost)

    results = {'a_in_graph':a_in_graph, 'a_out_graph': a_out_graph, 'a_out_attack_graph':a_out_attack_graph,
               'b_in_graph':b_in_graph, 'b_out_graph':b_out_graph, 'b_out_attack_graph':b_out_attack_graph,
               'prop_time_graph':prop_time_graph,'ratePatch_graph':ratePatch_graph,
               'generations':generations}
        
    return results

def plot_results(res, list_size, speed, numgen, attack_rate, lethality, u, environment, updatePrey, updatePred, init_state, prey_move_cost, pred_move_cost, pc):
    """
    Graphs
    """
    date = datetime.date.today()
    plt.close()
    #plt.xkcd(scale=1, length=150, randomness=2)
    widthline = 2    
    
    fig, ((ax1,ax2), (ax3,ax4), (ax5, ax6), (ax7, ax8)) = plt.subplots (4,2, sharex = True, sharey = False, figsize = (15, 15))
        
    fig.text(0.51,0.03, "Generation", ha = "center", fontsize = 20)
    fig.text(0.06,0.65, "Rate", fontsize = 20, rotation = "vertical")
    fig.text(0.06,0.25, "Proportion", fontsize = 15, rotation = "vertical")
    fig.text(0.5,0.25, "Return", fontsize = 15, rotation = "vertical")
    fig.text(0.8, 0.02, "riskyRefuge", ha = "center", fontsize = 10)    
    fig.text(0.9,0.02, time.asctime(time.localtime(time.time()) ), ha = "center", fontsize = 10) 
    
    ymaximum = max(res['a_in_graph'] + res['b_in_graph'] + res['a_out_graph'] + res['b_out_graph'] + 
                    res['a_out_attack_graph'] + res['b_out_attack_graph']) * 1.1  
    
    ax1.plot(res['generations'], res['a_in_graph'], linewidth = widthline)
    ax1.set_ylim(ymin=0, ymax=ymaximum)
    ax1.set_title("prey_in")
    
    ax2.plot(res['generations'], res['b_in_graph'], "r", linewidth = widthline)
    ax2.set_ylim(ymin=0, ymax=ymaximum)
    ax2.set_title("pred_in")

    ax3.plot(res['generations'], res['a_out_graph'], linewidth = widthline)
    ax3.set_ylim(ymin=0, ymax=ymaximum)
    ax3.set_title("prey_out")

    ax4.plot(res['generations'], res['b_out_graph'], "r", linewidth = widthline)
    ax4.set_ylim(ymin=0, ymax=ymaximum)
    ax4.set_title("pred_out")

    ax5.plot(res['generations'], res['a_out_attack_graph'], linewidth = widthline)
    ax5.set_ylim(ymin=0, ymax=ymaximum)
    ax5.set_title("prey_out after attack")

    ax6.plot(res['generations'], res['b_out_attack_graph'], "r", linewidth = widthline)
    ax6.set_ylim(ymin=0, ymax=ymaximum)
    ax6.set_title("pred_out after attack")
    
    state0,state1,state2,state3,state4,state5 = zip(*res['prop_time_graph'])
    ax7.set_title("Proportion of time in each state")
    state_with_colors_and_labels = (
        (state0, 'blue', 'prey_ref/pred_env'),
        (state1, 'red', 'prey_open/pred_env'),
        (state2, 'green', 'prey_ref/pred_open'),
        (state3, 'black', 'prey_open/pred_open'),
        (state4, 'gray', 'prey_ref(atk)/pred_env'),
        (state5, 'magenta', 'prey_ref(atk)/pred_open'))
    for state,color,label in state_with_colors_and_labels:   
        ax7.plot(res['generations'], np.array(state), color = color, label = label, linewidth = widthline)
    ax7.legend(loc = 9, bbox_to_anchor = (0.5,-0.07), ncol = 2, fontsize = 11,fancybox=True)
    
    ax8.set_title("Predator's return from Open")
    ax8.plot(res['generations'], res['ratePatch_graph'], "r", label = "Patch rate", linewidth = widthline)
    #ax8.set_ylim(ymin=0)
    ax8.axhline(environment, color='k', label = "Env rate", linewidth = widthline) 
    ax8.legend(loc = 9, bbox_to_anchor = (0.5,-0.05), ncol = 2, fancybox=True)
 
    ax1.grid(),ax2.grid(),ax3.grid(),ax4.grid(),ax5.grid(),ax6.grid(),ax7.grid(),ax8.grid()

    plt.suptitle("| list_size = %3.0f | speed = %3.0f | numgen = %3.0f | update = %3.1f, %3.1f | attack_rate = %3.2f | lethality = %3.2f | \n | riskRefuge = %3.1f | environment = %3.2f| init_state = %3.2f| prey_move_cost =%3.3f | pred_move_cost = %3.3f |" 
                % (list_size, speed, numgen, updatePrey, updatePred, attack_rate, lethality, u, environment, init_state, prey_move_cost, pred_move_cost))    
    
    sequence = "0"
    
    #this allows for printing on two different computers with different file directory structures. A cosmetic function for the authors.
    if pc == True:
        
            while isfile("C:\Users\pcain1\OneDrive\EgretActivePC\Plots\Plot_numgen%1.0f_AR%3.2f_ENV%3.2f_Lethality%3.2f_%s_%s.jpeg" % (numgen, attack_rate, environment, lethality, date, sequence)):
                sequence = int(sequence or "0") + 1
            filename = "C:\Users\pcain1\OneDrive\EgretActivePC\Plots\Plot_numgen%1.0f_AR%3.2f_ENV%3.2f_Lethality%3.2f_%s_%s.jpeg" % (numgen, attack_rate, environment, lethality, date, sequence)
            pickle.dump(res, open("C:\Users\pcain1\OneDrive\EgretActivePC\Pickles\Plot_numgen%1.0f_AR%3.2f_ENV%3.2f_Lethality%3.2f_%s_%s.p" 
                                  % (numgen, attack_rate, environment, lethality, date, sequence), "wb"))
            
    else:
            while isfile("/Users/PCain/Library/Mobile Documents/com~apple~CloudDocs/PythonScripts/adaptiveDynamics/EgretActive/Plots/Plot_numgen%1.0f_AR%3.2f_ENV%3.2f_Lethality%3.2f_%s_%s.jpeg" % (numgen, attack_rate, environment, lethality, date, sequence)):
                sequence = int(sequence or "0") + 1
            filename = "/Users/PCain/Library/Mobile Documents/com~apple~CloudDocs/PythonScripts/adaptiveDynamics/EgretActive/Plots/Plot_numgen%1.0f_AR%3.2f_ENV%3.2f_Lethality%3.2f_%s_%s.jpeg" % (numgen, attack_rate, environment, lethality, date, sequence)
    
            pickle.dump(res, open("/Users/PCain/Library/Mobile Documents/com~apple~CloudDocs/PythonScripts/adaptiveDynamics/EgretActive/Pickles/Plot_numgen%1.0f_AR%3.2f_ENV%3.2f_Lethality%3.2f_%s_%s.p" 
                                    % (numgen, attack_rate, environment, lethality, date, sequence), "wb"))
    plt.show()

    plt.savefig(filename)
 
    print "<Plot_numgen%1.0f_AR%3.2f_ENV%3.2f_Lethality%3.2f_%s_%s.p> was made." % (numgen, attack_rate, environment, lethality, date, sequence)
    print ("Done")
  
def plot_endpoints(values, numgen, attack_rate, lethality, u, environment, updatePrey, updatePred,
                   a_in_endpoints, a_out_endpoints, b_in_endpoints, b_out_endpoints, a_out_attack_endpoints, b_out_attack_endpoints, testing_parameter, prey_move_cost, pred_move_cost, pc):
    """
    Graphs
    """
    date = datetime.date.today()

    plt.close()
    #plt.xkcd(scale=1, length=150, randomness=2)
    widthline = 2    
    
    ymaximum = max(a_in_endpoints + a_out_endpoints + b_in_endpoints + b_out_endpoints + a_out_attack_endpoints + b_out_attack_endpoints) * 1.1      
    
    fig, ((ax1,ax2), (ax3,ax4), (ax5,ax6)) = plt.subplots (3,2, sharex = True, sharey = False, figsize = (15, 15))
        
    fig.text(0.51,0.03, testing_parameter, ha = "center", fontsize = 20)
    fig.text(0.06,0.5, "Rate", fontsize = 20, rotation = "vertical")
    fig.text(0.9,0.02, time.asctime( time.localtime(time.time()) ), ha = "center", fontsize = 10)
    fig.text(0.9,0.05, "Number of Points=" + str(len(values)), ha = "center", fontsize = 10)
       
    ax1.plot(values, a_in_endpoints, linewidth = widthline)
    ax1.set_ylim(ymin=0, ymax=ymaximum)
    ax1.set_title("prey_in")

    ax2.plot(values, b_in_endpoints, "r", linewidth = widthline)
    ax2.set_ylim(ymin=0, ymax=ymaximum)
    ax2.set_title("predator_in")

    ax3.plot(values, a_out_endpoints, linewidth = widthline)
    ax3.set_ylim(ymin=0, ymax=ymaximum)
    ax3.set_title("prey_out")

    ax4.plot(values, b_out_endpoints, "r", linewidth = widthline)
    ax4.set_ylim(ymin=0, ymax=ymaximum)
    ax4.set_title("predator_out")

    ax5.plot(values, a_out_attack_endpoints, linewidth = widthline)
    ax5.set_ylim(ymin=0, ymax=ymaximum)
    ax5.set_title("prey_out_attack")

    ax6.plot(values, b_out_attack_endpoints, "r", linewidth = widthline)
    ax6.set_ylim(ymin=0, ymax=ymaximum)
    ax6.set_title("predator_out_attack")

#    plt.suptitle("| generations = %3.0f | attack_rate = %3.1f | lethality = %3.1f | refugeRisk = %3.2f | environment = %3.2f | update = %3.1f, %3.1f |" 
#                % (numgen, attack_rate, lethality, u, environment, updatePrey, updatePred))    
    plt.suptitle("| generations = %3.0f | attack_rate = %3.1f | Lethality = %3.1f | refugeRisk = %3.2f | environment = %3.2f | update = %3.1f, %3.1f |\n | prey_move_cost = %3.2f | pred_move_cost = %3.2f |" 
                 % (numgen, attack_rate, lethality, u, environment, updatePrey, updatePred, prey_move_cost, pred_move_cost))   
    
    results = {'a_in_endpoints':a_in_endpoints, 'a_out_endpoints': a_out_endpoints, 'a_out_attack_endpoints':a_out_attack_endpoints,
               'b_in_endpoints':b_in_endpoints, 'b_out_endpoints':b_out_endpoints, 'b_out_attack_endpoints':b_out_attack_endpoints,
               'values':values}    
    
    sequence = "0"
    
    if pc == True:
        while isfile("C:\Users\pcain1\OneDrive\EgretActivePC\Endpoint_Plots\Endpoints_%s_numgen%1.0f_%s_%s.jpeg" % (testing_parameter, numgen, date, sequence)):
            sequence = int(sequence or "0") + 1
        filename = "C:\Users\pcain1\OneDrive\EgretActivePC\Endpoint_Plots\Endpoints_%s_numgen%1.0f_%s_%s.jpeg" % (testing_parameter, numgen, date, sequence)
       
        pickle.dump(results, open("C:\Users\pcain1\OneDrive\EgretActivePC\Pickles/Endpoints_%s_numgen%1.0f_%s_%s.p" 
                                % (testing_parameter, numgen, date, sequence), "wb"))

    else:
        while isfile("/Users/PCain/Library/Mobile Documents/com~apple~CloudDocs/PythonScripts/adaptiveDynamics/EgretActive/Endpoint_Plots/Endpoints_%s_numgen%1.0f_%s_%s.jpeg" % (testing_parameter, numgen, date, sequence)):
            sequence = int(sequence or "0") + 1
        filename = "/Users/PCain/Library/Mobile Documents/com~apple~CloudDocs/PythonScripts/adaptiveDynamics/EgretActive/Endpoint_Plots/Endpoints_%s_numgen%1.0f_%s_%s.jpeg" % (testing_parameter, numgen, date, sequence)
       
        pickle.dump(results, open("/Users/PCain/Library/Mobile Documents/com~apple~CloudDocs/PythonScripts/adaptiveDynamics/EgretActive/Pickles/Endpoints_%s_numgen%1.0f_%s_%s.p" 
                                % (testing_parameter, numgen, date, sequence), "wb"))
    
    
    plt.show()

    plt.savefig(filename)
    
    print "<Endpoints_%s_numgen%1.0f_%s_%s.p> was made." % (testing_parameter, numgen, date, sequence)

#---------------------------------------------------------------------------------------------------------------------#
#running script from main.py allows for much more functionality. 
if __name__ == "__main__":
    
    updatePrey, updatePred = False, True    
    
    #number of residents: total number is N + 1
    N = 0
    
    #-----------#
    # rate of state change for resident prey
    a_in_r = 1.0
    a_out_r = 1.0
    a_out_attack = 1.0
    

    # rate of state change resident predator
    b_in_r = 1.0
    b_out_r = 1.0
    b_out_attack = 1.0
    #-----------#
        
    # For use in fitness() functions
    init_state = 2
    food = 1.0
    k0 = 0.1                    # base probability of predator killing a prey
    k1 = 0.0                    # effect of N on prob that predator kills prey during attack (dilution)
    eta0 = 0.5                  # effect of N on probability that predator detects group
    beta0 = 0.2                 # probability an individual prey detects the predator
    alpha = 150                 # predator's area of search (rate of detection)
    comp = 0.0                  # competition effect for food
    
    #for pred_fitness()
    environment = 0.24    
    
    # to replace mu()
    attack_rate = 2.0           # probability of predator attacking
    lethality = 0.5     # probability of pred making succesful attack
    kill_rate = attack_rate * lethality
    prey_move_cost= 0.1 
    pred_move_cost = 0.01
    u = 0.0                     #risk of predation in refuge, a proportional value of kill_rate in patch
    
    # make the list size an odd number so that xdelt gets middle number
    list_size = 41
    
    # specify a constant that represents the relative rate or speed of change
    # in the phenotype
    speed = 100  
    
    # specify number of generations
    numgen = 50
    
    results = predpreydyn(N, a_in_r, a_out_r, a_out_attack, b_in_r, b_out_r, b_out_attack, k0, k1, eta0, beta0, alpha, comp, init_state,
                food, environment, list_size, speed, numgen, attack_rate, lethality, u, kill_rate, prey_move_cost, pred_move_cost, updatePrey, updatePred)

    plot_results(results, numgen, attack_rate, lethality, environment, updatePrey, updatePred)
    #pickle.dump(results, open( "/Users/PCain/Documents/OneDrive/PythonScripts/adaptiveDynamics/EgretActive/Pickles/Pickle_numgen%3.2f_AR%3.2f.p", "wb" ))     