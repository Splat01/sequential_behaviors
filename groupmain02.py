# -*- coding: utf-8 -*-
"""
Created on Fri Jan 30 15:58:40 2015
Does the adaptive (behavioral) dynamics for the case of one predator hunting
in two ponds.

Can modify any of the input parameters, or initial values of evolving parameters
@author: wmitchell
"""
import numpy as np
import matplotlib.pyplot as plt
from groupdyn02 import ab_dyn, shelve_results, plot_results

# Input Parameters: The following parameters influence the adaptive dynamics

k0list = [0.1, 0.1]         # base probability of predator killing a prey
k1 = 0.0                    # effect of N on prob that predator kills prey during attack (dilution)
eta0 = 0.5                  # effect of N on probability that predator detects group
beta0 = 0.2                 # probability an individual prey detects the predator
alpha = 150                 # predator's area of search (rate of detection)
comp = 0.0                  # competition effect for food
foodlist = [1.0,1.0]        # food levels in each patch
  
pred_response = 'True'      # a switch that indicates whether the predator's
                            #   behavior is responsive.

N1 = 3                      # Number of fish in pond 1. can be 1, 2 or 3
N2 = 2                      # Number of fish in pond 2. Can be 1 or 2 (not 3; should fix this)

# Evolving Parameters: The following parameters can 'evolve'  
# ar1, br1 and cr1 are fish parameters for patch 1
# ar2, br2 and cr2 are fish parameters for patch 2

ar1 = 1.0                   # self-initiated rate of movement from refuge to open
br1 = 1.0                   # self-initiated rate of movement from open to refuge
cr1 = 0.0                   # 'grouping' parameter: positive produces contagious
                            #       distribution, negative produces repulsion
ar2 = 1.0
br2 = 1.0
cr2 = 0.0

p = 0.5                 # Initial proportion of predator time allocated to patch 1


# Run the adaptive dynamics and store in dict called 'res'.
# (Possibly) shelve results; not necessary unless want to keep results on 
#     hard drive.
# Plot results.
#plot_results(res, N1, N2, k0list, k1, eta0, beta0, alpha, pred_response, comp)

#res = ab_dyn(ar1,br1,cr1,ar2,br2,cr2,p,N1,N2,k0list,k1,eta0,beta0,alpha,
#             foodlist,comp,pred_response,numsteps= 10)

#----- This section will: ----- 
#1. initialize list values to be tested (k1 to begin with)
#2. initialize list for results over iterative runs
#3. loop model to run for each of the values in values list
#4. append the value from each run into results list

#-- Testing k0 --#
#parameter_list = [[0.1, 0.1], [0.2, 0.2], [0.3, 0.3]]
# for x-axis

#-- Testing k1, comp, eta0, alpha (with changes) --#
parameter_list = np.arange(0, 300, 50)
iterative_results = []

for TEST in parameter_list:
    res = ab_dyn(ar1,br1,cr1,ar2,br2,cr2,p,N1,N2,k0list,k1,eta0,beta0,TEST,
             foodlist,comp,pred_response,numsteps= 1000)
    iterative_results.append(res['p_result'][-1])
    #print res['p_result']

print ""    
print "results list:", iterative_results

plt.plot(parameter_list, iterative_results, 'ko')
plt.ylabel('last p_result value')
plt.xlabel('parameter value')
plt.title('predator\'s area of search (rate of detection, alpha) ')
plt.show()
 

