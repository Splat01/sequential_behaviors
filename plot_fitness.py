# -*- coding: utf-8 -*-
"""
Created on Tue Sep 04 13:48:03 2018

@author: pcain1
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Jul 14 16:12:15 2016

Branch: riskyRefuge

@author: PCain
"""

import pickle
import matplotlib.pyplot as plt
from adapt_dyn import prey_fitness, pred_fitness
import numpy as np


#test = "environment"
test = "lethality"
N=0

# For use in fitness() functions
init_state = 0.0 
food = 1.0
    
k0 = 0.1                    # base probability of predator killing a prey
k1 = 0.0                    # effect of N on prob that predator kills prey during attack (dilution)
eta0 = 0.5                  # effect of N on probability that predator detects group
beta0 = 0.2                 # probability an individual prey detects the predator
alpha = 150                 # predator's area of search (rate of detection)
comp = 0.0                  # competition effect for food

attack_rate = 1.0
numgen = 2000
lethality = 0.0
kill_rate = attack_rate * lethality
u = 0.0                     #risk of predation in refuge, a proportional value of kill_rate in patch
pred_move_cost = 0.01
prey_move_cost = 0.005

environment1, environment2, environment3 = 0.6, 1.0, 3.0

number_points = 8
ymaximum = 3


#---------------------------------------------------------------------------------------------#
# This is graph used in first manuscript
u = 0.00
pickleFile1 = "C:\Users\pcain1\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen8000_2018-05-16_0.p"    #Low environment    FULL GAME
pickleFile2 = "C:\Users\pcain1\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen8000_2018-05-16_4.p"    #Med environment    FULL GAME
pickleFile3 = "C:\Users\pcain1\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen8000_2018-05-22_0.p"    #High Environment   FULL GAME

#pickleFile1 = "C:\Users\pcain1\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen2000_2018-08-26_1.p"    #Low environment NO-INFO
#pickleFile2 = "C:\Users\pcain1\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen2000_2018-08-26_0.p"    #Med environment
#pickleFile3 = "C:\Users\pcain1\OneDrive\EgretActivePC\Pickles\Endpoints_lethality_numgen2000_2018-08-27_0.p"    #High Environment

#---------------------------------------------------------------------------------------------#
#u = 0.1
#pickleFile1 = "Endpoints_lethality_numgen12000_2016-06-23_0.p"         #Temp File
#pickleFile2 = "Endpoints_lethality_numgen12000_2016-07-14_0.p"         #Temp File
#pickleFile3 = "Endpoints_lethality_numgen12000_2016-07-11_0.p"         #Temp File
#---------------------------------------------------------------------------------------------#
#u = 0.05
#pickleFile1 = "Endpoints_lethality_numgen12000_2016-06-23_1.p"         #Temp File
#pickleFile2 = "Endpoints_lethality_numgen12000_2016-06-24_0.p"         #Temp File
#pickleFile3 = "Endpoints_lethality_numgen12000_2016-07-01_0.p"         #Temp File
#---------------------------------------------------------------------------------------------#
#u = 0.01
#pickleFile1 = "Endpoints_lethality_numgen12000_2016-06-21_0.p"         #Temp File
#pickleFile2 = "Endpoints_lethality_numgen12000_2016-06-21_1.p"         #Temp File
#pickleFile3 = "Endpoints_lethality_numgen12000_2016-06-22_0.p"         #Temp File
#---------------------------------------------------------------------------------------------#


plt.close()
#plt.xkcd(scale=0.5, length=150, randomness=0.5)

widthline = 3
hfont = {'fontname':'monospace'}

#unpack the pickles and separate the behavior rates
points1, points2, points3 = pickle.load(open(pickleFile1,"rb")), pickle.load(open(pickleFile2,"rb")), pickle.load(open(pickleFile3,"rb"))

a_in_end1 = points1["a_in_endpoints"]
b_in_end1 = points1["b_in_endpoints"]
b_out_end1 = points1["b_out_endpoints"]
a_out_end1 = points1["a_out_endpoints"]
a_out_atk_end1 = points1["a_out_attack_endpoints"]
b_out_atk_end1 = points1["b_out_attack_endpoints"]

a_in_end2 = points2["a_in_endpoints"]
b_in_end2 = points2["b_in_endpoints"]
b_out_end2 = points2["b_out_endpoints"]
a_out_end2 = points1["a_out_endpoints"]
a_out_atk_end2 = points2["a_out_attack_endpoints"]
b_out_atk_end2 = points2["b_out_attack_endpoints"]
    

a_in_end3 = points3["a_in_endpoints"]
b_in_end3 = points3["b_in_endpoints"]
b_out_end3 = points3["b_out_endpoints"]
a_out_end3 = points3["a_out_endpoints"]
a_out_atk_end3 = points3["a_out_attack_endpoints"]
b_out_atk_end3 = points3["b_out_attack_endpoints"]


length = np.arange(1, 8.1, 1)
#length = range(1,number_points+0.1,1)

fitnessPred1, fitnessPred2, fitnessPred3, fitnessPrey1,fitnessPrey2, fitnessPrey3 = [],[],[],[],[],[]



for i in np.arange(0,8,1):
    kill_rate = attack_rate * i
    fitnessPred1.append(pred_fitness(N,points1["a_in_endpoints"][i],points1["a_out_endpoints"][i],points1["a_out_attack_endpoints"][i],
                                     points1["b_in_endpoints"][i],points1["b_out_endpoints"][i], points1["b_out_attack_endpoints"][i],
                                     environment1, attack_rate, kill_rate, u, pred_move_cost))
    fitnessPred2.append(pred_fitness(N,points2["a_in_endpoints"][i],points2["a_out_endpoints"][i],points2["a_out_attack_endpoints"][i],
                                     points2["b_in_endpoints"][i],points2["b_out_endpoints"][i],points2["b_out_attack_endpoints"][i],
                                     environment1, attack_rate, kill_rate, u, pred_move_cost))
    fitnessPred3.append(pred_fitness(N,points3["a_in_endpoints"][i],points3["a_out_endpoints"][i],points3["a_out_attack_endpoints"][i],
                                     points3["b_in_endpoints"][i],points3["b_out_endpoints"][i],points3["b_out_attack_endpoints"][i],
                                     environment1, attack_rate, kill_rate, u, pred_move_cost))
    fitnessPrey1.append(prey_fitness(N,points1["a_in_endpoints"][i],points1["a_out_endpoints"][i],
                                     points1["a_in_endpoints"][i],points1["a_out_endpoints"][i],points1["a_out_attack_endpoints"][i],
                                     points1["b_in_endpoints"][i],points1["b_out_endpoints"][i],points1["b_out_attack_endpoints"][i],
                                     attack_rate, kill_rate, u, comp, init_state, food, prey_move_cost))
    fitnessPrey2.append(prey_fitness(N,points2["a_in_endpoints"][i],points2["a_out_endpoints"][i],
                                     points2["a_in_endpoints"][i],points2["a_out_endpoints"][i],points2["a_out_attack_endpoints"][i],
                                     points2["b_in_endpoints"][i],points2["b_out_endpoints"][i],points2["b_out_attack_endpoints"][i],
                                     attack_rate, kill_rate, u, comp, init_state, food, prey_move_cost))
    fitnessPrey3.append(prey_fitness(N,points3["a_in_endpoints"][i],points3["a_out_endpoints"][i],
                                     points3["a_in_endpoints"][i],points3["a_out_endpoints"][i],points3["a_out_attack_endpoints"][i],
                                     points3["b_in_endpoints"][i],points3["b_out_endpoints"][i],points3["b_out_attack_endpoints"][i],
                                     attack_rate, kill_rate, u, comp, init_state, food, prey_move_cost))
    

fig, ((ax1,ax2,ax3)) = plt.subplots(1,3, sharex = True, sharey = False, figsize = (20,10))

ax1.plot(length, fitnessPred1, "k", linewidth = widthline, label = "Pred Fitness")
ax1.plot(length, fitnessPrey1, "b", linewidth = widthline, label = "Prey Fitness")
ax1.set_ylim(ymin=0, ymax=ymaximum)
ax1.legend(loc = 0, fontsize = "small", fancybox = True)
ax1.set_xlabel("Low MOC", fontsize = 25)
ax1.xaxis.set_label_coords(0.5, 1.2)
ax1.set_ylabel("Fitness", fontsize = 15, rotation = "vertical", **hfont)
ax1.minorticks_on()
ax1.tick_params('x', width=2, which='major')
ax1.tick_params('x', width=2, which='minor')

ax2.plot(length, fitnessPred2, "k", linewidth = widthline, label = "Pred Fitness")
ax2.plot(length, fitnessPrey2, "b", linewidth = widthline, label = "Prey Fitness")
ax2.set_ylim(ymin=0, ymax=ymaximum)
ax2.set_xlabel("Medium MOC", fontsize = 25)
ax2.xaxis.set_label_coords(0.5, 1.2)
ax2.tick_params('x', width=2, which='major')
ax2.tick_params('x', width=2, which='minor')

ax3.plot(length, fitnessPred3, "k", linewidth = widthline, label = "Pred Fitness")
ax3.plot(length, fitnessPrey3, "b", linewidth = widthline, label = "Prey Fitness")
ax3.set_ylim(ymin=0, ymax=ymaximum)
ax3.set_xlabel("High MOC", fontsize = 25)
ax3.xaxis.set_label_coords(0.5, 1.2)
ax3.tick_params('x', width=2, which='major')
ax3.tick_params('x', width=2, which='minor')



plt.show()


