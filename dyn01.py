# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 14:46:17 2015

@author: wmitchell
"""

import numpy as np
import scipy 
from scipy import integrate
from matplotlib import pyplot as plt

def logistic(N,t):
    """
    dynamic for growth of a single population with intrinsic growth rate, r,
    and carring capacity, K.
    """
    r=1.
    K=100.
    return r*N*(1-N/K)
        
N0 = 1




t = np.linspace(1,50,50)

#out = integrate.odeint(logistic,N0,t)

#pop = [50,1]

a01 = 1.
a00 = -a01
a10 = 2.
a11 = -a10

A = [[a00, a01],\
     [a10, a11]]

eval, evecs = scipy.linalg.eig(A,left=True,right=False)
print scipy.matrix(A)
print eval
print evecs
print
state = [10,10]
print np.matrix(A)
print
print state
print "dot(A,state)"
print np.dot(A,state)
print "dot(state,A)"
print np.dot(state,A)


def distrib(Q):
    """
    Takes transition rate matrix, Q, and returns distribution among states.
    """
    evals,evecs = scipy.linalg.eig(Q,left=True,right=False)
    index = np.argmin(evals**2)
    return evecs[:,index]/sum(evecs[:,index])
    
dist = distrib(A)
print "distribution of states: ", dist

def markov_dyn(state,t):
    """
    """
   
    return np.dot(state,A)
    
state0 = [0.5,0.5]
    
out = integrate.odeint(markov_dyn,state0,t)
print out[-5:]
    


#out = integrate.odeint(predprey, pop, t)

#print out
plt.close()

plt.subplot(121)
plt.plot(t,t)
plt.xlabel("this")
plt.ylabel("or something")
plt.subplot(122)
plt.plot(t,out)
plt.xlabel("that")
plt.grid()
plt.show()

#print plt.style.available
#with plt.style.context("bmh"):
#    plt.plot(t,out)
#    plt.show()