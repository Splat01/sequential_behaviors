# -*- coding: utf-8 -*-
"""
Created on Thu May 14 13:10:58 2015

Branch: riskyRefuge

@author: Patrick Cain and William Mitchell
"""

import numpy as np
import timeit
from adapt_dyn import predpreydyn, plot_results, plot_endpoints
#import os
import time

#allows for game or no-game. False holds that player static, true allows it to evolve
updatePrey, updatePred = False, True

#number of residents: total number is N + 1
N = 0

#----intial behavioral rates of state change-------#
# rate of state change for resident prey
a_in_r = 1.0
a_out_r = 1.0
a_out_attack = 1.0

# rate of state change resident predator
b_in_r = 1.0
b_out_r = 1.0
b_out_attack = 1.0
#-----------#

# For use in fitness() functions
init_state = 0.0 
food = 1.0
    
k0 = 0.1                    # base probability of predator killing a prey
k1 = 0.0                    # effect of N on prob that predator kills prey during attack (dilution)
eta0 = 0.5                  # effect of N on probability that predator detects group
beta0 = 0.2                 # probability an individual prey detects the predator
alpha = 150                 # predator's area of search (rate of detection)
comp = 0.0                  # competition effect for food

#for pred_fitness() 
environment = 3.0

# to replace mu()
attack_rate = 1.0                 # predator attack rate (how long it waits to attack)
lethality = 4.0                   # probability of pred making succesful attack
kill_rate = attack_rate * lethality
u = 0.0                     #risk of predation in refuge, a proportional value of kill_rate in patch
pred_move_cost = 0.01
prey_move_cost = 0.005

# make the list size an odd number so that xdelt gets middle number
list_size = 191

# a constant that represents the relative rate or speed of change in the phenotype
speed = 3000    

# specify number of generations
numgen = 10

# specify max rate for pred_out_attack. 
rateCapMax_predOutAttack = 30


#one_run_plot, end_points_plot = True, False
one_run_plot, end_points_plot = False, True


pc = True

#----------------# One run plot #----------------#    
if one_run_plot == True:    
    print time.asctime(time.localtime(time.time()))    
    start = timeit.default_timer()
    res = predpreydyn(N, a_in_r, a_out_r, a_out_attack, b_in_r, b_out_r, b_out_attack, k0, k1, eta0, beta0, alpha, comp, init_state,
                    food, environment, list_size, speed, numgen, attack_rate, lethality, kill_rate, u, prey_move_cost, pred_move_cost, updatePrey, updatePred, rateCapMax_predOutAttack)
    stop = timeit.default_timer()
    
    plot_results(res, list_size, speed, numgen, attack_rate, lethality, u, environment, updatePrey, updatePred, init_state, prey_move_cost, pred_move_cost, pc) 
    
    print "The test is done. it ran for %d minutes." % (int(stop - start) / 60)
#    os.system('say "the test is done. It ran for %d minutes."' % (int(stop - start) / 60))
    print time.asctime(time.localtime(time.time()))

#----------------# Plot of end points #----------------#
if end_points_plot == True:
    print "Testing across values...", time.asctime(time.localtime(time.time()))         
    print "numgen = %3.0f | attack_rate = %3.1f | refugeRisk = %3.2f | env = %3.2f \n | pred_move_cost = %3.3f | pred_move_cost = %3.3f" % (numgen, attack_rate, u, environment, prey_move_cost, pred_move_cost)
    testing_parameter = "lethality"
    values = np.arange(1.0, 8.1, 0.1)     #values of lethality to measure each run at
    a_in_endpoints, a_out_endpoints, b_in_endpoints, b_out_endpoints, a_out_attack_endpoints, b_out_attack_endpoints = [],[],[],[],[],[]
       
    
    start = timeit.default_timer()
    for i in values:
        kill_rate = attack_rate * i
        res = predpreydyn(N, a_in_r, a_out_r, a_out_attack, b_in_r, b_out_r, b_out_attack, k0, k1, eta0, beta0, alpha, comp, init_state,
                          food, environment, list_size, speed, numgen, attack_rate, i, kill_rate, u, prey_move_cost, pred_move_cost, updatePrey, updatePred, rateCapMax_predOutAttack)
        a_in_endpoints.append(res['a_in_graph'][-1])
        a_out_endpoints.append(res['a_out_graph'][-1])
        a_out_attack_endpoints.append(res['a_out_attack_graph'][-1])         
        b_in_endpoints.append(res['b_in_graph'][-1])
        b_out_endpoints.append(res['b_out_graph'][-1])
        b_out_attack_endpoints.append(res['b_out_attack_graph'][-1])
        print "Finished", i, time.asctime(time.localtime(time.time()))
    stop = timeit.default_timer()    
    
    plot_endpoints(values, numgen, attack_rate, lethality, u, environment, updatePrey, updatePred, 
                   a_in_endpoints, a_out_endpoints, b_in_endpoints, b_out_endpoints, a_out_attack_endpoints, b_out_attack_endpoints, testing_parameter, prey_move_cost, pred_move_cost, pc)
    
    print "The %s test is done. it ran for %d minutes." % (testing_parameter, int(stop - start) / 60)               
#    os.system('say "the %s test is done. it ran for %d minutes.' % (testing_parameter, int(stop - start) / 60))
    print time.asctime(time.localtime(time.time()))

         