# -*- coding: utf-8 -*-
"""
Created on Wednesday Oct 29, 2014
Added ability to vary food for prey between patches
Added ability to easily let killing efficiency (k0,k1) vary between patches 
@author: wmitchell
Modify plotting function to handle greater range of N1
"""
import numpy as np
import shelve
import matplotlib.pyplot as plt
from groupfuncs05 import *

def ab_dyn(ar1,br1,cr1,ar2,br2,cr2,p,N1,N2,k0list,k1,eta0,beta0,alpha,
           foodlist,comp,pred_response,numsteps,err=1e-6):
    """
    Dynamics of prey parameters a, b, c; and predator parameter p
    """
    print
    dmult = 10              # parameter than varies the rate of dynamics
                            #   can be changed if too slow or fast (or crazy)
    a1 = [ar1]*N1
    b1 = [br1]*N1
    c1 = [cr1]*N1
    
    a2 = [ar2]*N2
    b2 = [br2]*N2
    c2 = [cr2]*N2    
    
    a1_result = []
    b1_result = []
    c1_result = []
    
    a2_result = []
    b2_result = []
    c2_result = []
    
    list_size = 11      # used in getting mesh points
    
    a1_prop = []
    a2_prop = []
    state1_10 = []
    state1_11 = []
    state1_20 = []
    state1_21 = []
    state1_22 = []
#    state1_23 = []
    state1_30 = []
    state1_31 = []
    state1_32 = []
    state1_33 = []
    state2_10 = []
    state2_11 = []
    state2_20 = []
    state2_21 = []
    state2_22 = []
    
    p_result = []
    
    
    fit1 = []
    fit2 = []
    
    for n in range(numsteps):
        print 'n:', n
        
        # first calculate new values of a,b for patch 1
        k0 = k0list[0]
        food = foodlist[0]
        
        # generate list of a values for patch 1
        a1list = np.linspace(ar1-0.5,ar1+0.5,list_size)
        
        # genereate list of fitnesses for each a value
        a1fit = [prey_fitness([a0]+[ar1]*(N1-1),b1,c1,p,N1,k0,k1,eta0,beta0,alpha,food,comp) 
                for a0 in a1list]

        a1fit = np.array(a1fit)
        a1grad = np.gradient(a1fit)
        a1delt = a1grad[list_size/2]*dmult
        print 'a1delt: ', a1delt
        
        newa1 = np.clip(ar1 + a1delt,0,100)
        
        b1list = np.linspace(br1-0.5,br1+0.5,list_size)
        b1fit = [prey_fitness(a1,[b0]+[br1]*(N1-1),c1,p,N1,k0,k1,eta0,beta0,alpha,food,comp) 
                for b0 in b1list]
        b1fit = np.array(b1fit)
        b1grad = np.gradient(b1fit)
        b1delt = b1grad[list_size/2]*dmult
        
        newb1 = np.clip(br1 + b1delt,0,100)   
        
        c1list = np.linspace(cr1-0.05,cr1+0.05,list_size)
        c1fit = [prey_fitness(a1,b1,[c0]+[cr1]*(N1-1),p,N1,k0,k1,eta0,beta0,alpha,food,comp) 
                for c0 in c1list]
        c1fit = np.array(c1fit)
        c1grad = np.gradient(c1fit)
        c1delt = c1grad[list_size/2]*dmult
        
        print 'c1delt', c1delt
        
        newc1 = np.clip(cr1+c1delt,-5.,5.)
        
        fit1_gen = prey_fitness(a1,b1,c1,p,N1,k0,k1,eta0,beta0,alpha,food,comp)
        fit1.append(fit1_gen)
        
        print 'k0 in patch 1: ', k0
        
        # Now do for patch 2
        k0 = k0list[1]
        food = foodlist[1]
        
        a2list = np.linspace(ar2-0.5,ar2+0.5,list_size)
        a2fit = [prey_fitness([a0]+[ar2]*(N2-1),b2,c2,1.-p,N2,k0,k1,eta0,beta0,alpha,food,comp) 
                for a0 in a2list]
        a2fit = np.array(a2fit)
        a2grad = np.gradient(a2fit)
        a2delt = a2grad[list_size/2]*dmult
        
        newa2 = np.clip(ar2 + a2delt,0,100)

        
        b2list = np.linspace(br2-0.5,br2+0.5,list_size)
        b2fit = [prey_fitness(a2,[b0]+[br2]*(N2-1),c2,1.-p,N2,k0,k1,eta0,beta0,alpha,food,comp) 
                for b0 in b2list]
        b2fit = np.array(b2fit)
        b2grad = np.gradient(b2fit)
        b2delt = b2grad[list_size/2]*dmult
        
        newb2 = np.clip(br2 + b2delt,0,100)   
        
#        ar = np.real(newa)
        print 'newa1, newa2: ', newa1, newa2
        print 'newb1, newb2: ', newb1, newb2
#        print 'a1/b1: ', ar1/br1
                
        
        c2list = np.linspace(cr2-0.05,cr2+0.05,list_size)
        c2fit = [prey_fitness(a2,b2,[c0]+[cr2]*(N2-1),1.-p,N2,k0,k1,eta0,beta0,alpha,food,comp) 
                for c0 in c2list]
        c2fit = np.array(c2fit)
        c2grad = np.gradient(c2fit)
        c2delt = c2grad[list_size/2]*dmult
        print 'c2delt', c2delt
        
        newc2 = np.clip(cr2+c2delt,-5.,5.)
        
        print 'newc2: ', newc2

        fit2_gen = prey_fitness(a2,b2,c2,1-p,N2,k0,k1,eta0,beta0,alpha,food,comp)
        fit2.append(fit2_gen)        
        
        print 'k0 in patch 2: ', k0

        # Now, calculate fitess of variations on predator time allocation
        plist = np.linspace(p-0.1,p+0.1,list_size)
        pfit = [predfit32k0(pf,a1,b1,c1,a2,b2,c2,N1,N2,k0list,k1,eta0,beta0,alpha) 
            for pf in plist]
        pfit = np.array(pfit)
        pgrad = np.gradient(pfit)
        pdelt = pgrad[list_size/2]      #*dmult           
        newp = np.clip(p + pdelt,0,1)
        print 'p,pdelt,newp', p,pdelt,newp


        print 'fit1: ', fit1_gen
        print 'fit2: ', fit2_gen
        
        if pred_response == 'True':
            p = newp
        
        ar1 = newa1     #np.real(newa1)
        ar2 = newa2     #np.real(newa2)
        br1 = newb1     #np.real(newb1)
        br2 = newb2     #np.real(newb2)
        cr1 = newc1     #np.real(newc1)
        cr2 = newc2     #np.real(newc2)
        
        print 'ar1, ar2: ', ar1, ar2
        print 'br1, br2: ', br1, br2
        print 'cr1, cr2: ', cr1, cr2
        
        a1_result.append(ar1)
        b1_result.append(br1)
        c1_result.append(cr1)

        a2_result.append(ar2)
        b2_result.append(br2)
        c2_result.append(cr2)
        
        p_result.append(p)
        
        a1 = [ar1]*N1
        b1 = [br1]*N1
        c1 = [cr1]*N1
        
        a2 = [ar2]*N2
        b2 = [br2]*N2
        c2 = [cr2]*N2 
        
        print 'a1 ', a1
        print 'a2 ', a2
        print 'b1 ', b1
        print 'b2 ', b2
        
        if N1==1:
            Q1 = matQ1(a1,b1,c1)
            dist1 = distrib(Q1)
            print 'dist1',dist1
            a1_prop.append(dist1[1])
            state1_10.append(dist1[0])
            state1_11.append(dist1[1])
            
            
        if N1==2:
            Q1 = matQ2(a1,b1,c1)
            dist1 = distrib(Q1)
            print 'dist1',dist1
            a1_prop.append(dist1[2]+dist1[3]) 
            state1_20.append(dist1[0])
            state1_21.append(dist1[1]+dist1[2])
            state1_22.append(dist1[3])
            
           
        if N1==3:
            Q1 = matQ3(a1,b1,c1)
            dist1 = distrib(Q1)
            print 'dist1',dist1
            a1_prop.append(dist1[3]+dist1[5]+dist1[6]+dist1[7])
#            print 'state30 ', dist1[0]
            state1_30.append(dist1[0])
            state1_31.append(dist1[1]+dist1[2]+dist1[3])
            state1_32.append(dist1[4]+dist1[5]+dist1[6])
            state1_33.append(dist1[7])
            
            
        if N2==1:
            Q2 = matQ2(a2,b2,c2)
            dist2 = distrib(Q2)
            print 'dist2',dist2
            a2_prop.append(dist2[1])
            state2_10.append(dist2[0])
            state2_11.append(dist2[1])
            
        if N2==2:
            Q2 = matQ2(a2,b2,c2)
            dist2 = distrib(Q2)
            print 'dist2',dist2
            a2_prop.append(dist2[2]+dist2[3]) 
            state2_20.append(dist2[0])
            state2_21.append(dist2[1]+dist2[2])
            state2_22.append(dist2[3])
            



#        Q2 = matQ2(a2,b2,c2)
#        dist2 = distrib(Q2)
#        print 'dist2',dist2
#        a2_prop.append(dist2[2]+dist2[3])
        
        print
        print
        
    result = {'k0list':k0list, 'k1':k1, 'eta0':eta0, 'beta0':beta0, 'alpha':alpha,
              'pred_response':pred_response, 'N1':N1, 'N2':N2, 
              'a1_prop':a1_prop, 'a2_prop':a2_prop, 'c1_result':c1_result,
              'c2_result':c2_result, 'p_result':p_result, 'fit1':fit1,
              'fit2':fit2, 'state1_10':state1_10, 'state1_11':state1_11,
              'state1_20':state1_20, 'state1_21':state1_21, 
              'state1_22':state1_22, 'state1_30':state1_30,
              'state1_31':state1_31, 'state1_32':state1_32, 'state1_33':state1_33,
              'state2_10':state2_10, 'state2_11':state2_11,
              'state2_20':state2_20,'state2_21':state2_21,'state2_22':state2_22} 
    return result
    
def plot_results(res, N1, N2, k0list, k1, eta0, beta0, alpha, pred_response, comp):
    """
    """
    import numpy as np
    import matplotlib
    from matplotlib import pyplot as plt
        
    
    tsteps = np.arange(len(res['c1_result']))
    
    plt.subplot(321)
    plt.plot(tsteps,res['c1_result'])
    plt.ylabel('prey association')
#    plt.text(600,0.4, '3 prey')
    plt.ylim(ymin= -3,ymax=3)
    plt.grid()
    plt.title("{} prey".format(N1))


    
    plt.subplot(322)
    plt.plot(tsteps,res['c2_result'])
    plt.ylabel('prey association')
#    plt.text(600,0.4, '2 prey')
    plt.ylim(ymin= -3,ymax=3)
    plt.grid()
    plt.title("{} prey".format(N2))


    
    plt.subplot(323)
#    plt.plot(tsteps,res['a1_prop'])
    if N1 == 3:
        plt.plot(tsteps,res['state1_30'],label='none')
        plt.plot(tsteps,res['state1_31'],label='1')
        plt.plot(tsteps,res['state1_32'],label='2')
        plt.plot(tsteps,res['state1_33'],label='3')
    elif N1 == 2:
        plt.plot(tsteps,res['state1_20'],label='none')
        plt.plot(tsteps,res['state1_21'],label='1')
        plt.plot(tsteps,res['state1_22'],label='2')
    elif N1 == 1:
        plt.plot(tsteps,res['state1_10'],label='none')
        plt.plot(tsteps,res['state1_11'],label='1')
    plt.legend(loc='upper left',labelspacing=0.2,prop={'size':8})
    plt.ylabel('prey distribution')
    plt.ylim(ymin=0.0,ymax=1.1)
#    plt.title('Patch with 3 Prey')
#    plt.text(600,0.4,'3 prey')
    plt.grid()

    plt.subplot(324)
#    plt.plot(tsteps,res['a2_prop'])
    if N2 == 2:
        plt.plot(tsteps,res['state2_20'],label='none')
        plt.plot(tsteps,res['state2_21'],label='1')
        plt.plot(tsteps,res['state2_22'],label='2')
    elif N2 == 1:
        plt.plot(tsteps,res['state2_10'],label='none')
        plt.plot(tsteps,res['state2_11'],label='1')
        
    plt.legend(loc='upper left',labelspacing=0.2,prop={'size':8})
        
        
    plt.ylabel('prey distribution')
    plt.ylim(ymin=0.0,ymax=1.1)
#    plt.title('Patch with 2 Prey')
#    plt.text(600,0.4, '2 prey')
    plt.grid()
    
    plt.subplot(325)
    plt.plot(tsteps,res['p_result'])
    plt.ylabel('predator allocation')
    plt.ylim(ymin=0.,ymax=1.1)
    plt.xlabel('generations')
    plt.grid()
    
    plt.subplot(326)
    plt.plot(tsteps, res['fit1'], label='fitness %i' % (N1))
    plt.plot(tsteps, res['fit2'], label='fitness %i' % (N2))
    plt.xlabel('generations')
    plt.ylabel('prey fitness')
    plt.grid()
    plt.yticks(np.arange(0.,0.6,0.1))
    plt.legend(loc='upper left',labelspacing=0.2,prop={'size':8})
    
#    plt.suptitle("Evolutionary Dynamics of Predator/Prey Parameters")
    plt.suptitle("k0_1=%3.1f,k0_2=%3.1f, k1=%3.1f, eta0=%3.1f, beta0=%3.1f, alpha=%3.1f, predresponse=%5s, comp=%3.1f" 
                % (k0list[0],k0list[1],k1,eta0,beta0,alpha,pred_response,comp))
    plt.subplots_adjust(top=0.85)
    plt.show()

#    plt.tight_layout()
#    plt.figure(figsize=(3,4))
  
#    plt.savefig("gm"+date_string+".png")

def shelve_results(res):
    """
    """        

    import shelve
    import datetime
    dt = datetime.datetime.now()
#    date_string = dt.strftime("%Y-%m-%d-%H-%M-%S.%f")
    date_string = dt.strftime("%Y-%m-%d-%H-%M-%S.%f")
    s = shelve.open('egret-'+date_string)
    s['results'] = res
    s.close()


   
if __name__=="__main__":
    """
    """
    k0list = [0.1, 0.2]                # base probability of predator killing a prey
    k1 = 0.0              # effect of N on prob that predator kills prey during attack (dilution)
    eta0 = 0.0             # effect of N on probability that predator detects group
    beta0 = 0.2             # probability an individual detects the predator
    alpha = 150              # predator's area of search
    comp = 0.0             # competition effect for food
    foodlist = [1.0,1.0]
       
    pred_response = 'True'    
    ar1 = 1.0               # self-initiated rate of movement from refuge to open
    br1 = 1.0               # self-initiated rate of movement from open to refuge
    cr1 = 0.0              # 'grouping' parameter: positive produces contagious
                            #     distribution, negative produces repulsion
    
    ar2 = 1.0
    br2 = 1.0
    cr2 = 0.0
    
    p = 0.5                 # proportion of predator time allocated to patch 1
    
    N1 = 3
    N2 = 2
    
    
    res = ab_dyn(ar1,br1,cr1,ar2,br2,cr2,p,N1,N2,k0list,k1,eta0,beta0,alpha,
                 foodlist,comp,pred_response,numsteps= 1000)
    shelve_results(res)
    plot_results(res)
#    plt.savefig("f1.pdf")
#    print("done")